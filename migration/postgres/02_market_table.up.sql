CREATE TABLE "market"(
    "id" UUID PRIMARY KEY,
    "name" VARCHAR NOT NULL,
    "branch_id" UUID REFERENCES "branch"("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);
