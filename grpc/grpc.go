package grpc

import (
	"gitlab.com/market/mk_go_content_service/config"
	"gitlab.com/market/mk_go_content_service/genproto/content_service"
	"gitlab.com/market/mk_go_content_service/grpc/client"
	"gitlab.com/market/mk_go_content_service/grpc/service"
	"gitlab.com/market/mk_go_content_service/pkg/logger"
	"gitlab.com/market/mk_go_content_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()
	content_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))
	content_service.RegisterMarketServiceServer(grpcServer, service.NewMarketService(cfg, log, strg, srvc))
	content_service.RegisterStaffServiceServer(grpcServer, service.NewStaffService(cfg, log, strg, srvc))
	content_service.RegisterDillerServiceServer(grpcServer, service.NewDillerService(cfg, log, strg, srvc))


	reflection.Register(grpcServer)
	return
}
