package service

import (
	"context"
	"fmt"

	"gitlab.com/market/mk_go_content_service/config"
	"gitlab.com/market/mk_go_content_service/genproto/content_service"
	"gitlab.com/market/mk_go_content_service/grpc/client"
	"gitlab.com/market/mk_go_content_service/pkg/logger"
	"gitlab.com/market/mk_go_content_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type BranchService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*content_service.UnimplementedBranchServiceServer
}

func NewBranchService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *BranchService {
	return &BranchService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (i *BranchService) Create(ctx context.Context, req *content_service.CreateBranch) (resp *content_service.Branch, err error) {
	i.log.Info("---------CreateBranch---------", logger.Any("req", req))

	id, err := i.strg.Branch().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateBranch->Branch->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Branch().GetByID(ctx, id)
	if err != nil {
		i.log.Error("!!!GetBranchById --> Create Response->Branch", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *BranchService) GetByID(ctx context.Context, req *content_service.BranchPrimaryKey) (resp *content_service.Branch, err error) {
	fmt.Println("klmcsldkcm lksdkcmsldkcmdslkcmsdklcmsdklcmdslkd")
	i.log.Info("---------GetBranchById---------", logger.Any("req", req))

	resp, err = i.strg.Branch().GetByID(ctx, req)
	if err != nil {
		i.log.Error("!!!GetBranchById->Branch->GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *BranchService) GetList(ctx context.Context, req *content_service.GetListBranchRequest) (resp *content_service.GetListBranchResponse, err error) {

	i.log.Info("---GetBranchs------>", logger.Any("req", req))

	resp, err = i.strg.Branch().GetList(ctx, req)
	if err != nil {
		i.log.Error("!!!GetBranchs->Branch->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *BranchService) Update(ctx context.Context, req *content_service.UpdateBranch) (resp *content_service.Branch, err error) {

	i.log.Info("---UpdateBranch------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Branch().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateBranch--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Branch().GetByID(ctx, &content_service.BranchPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetBranch->Branch->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *BranchService) Delete(ctx context.Context, req *content_service.BranchPrimaryKey) (resp *content_service.BranchEmpty, err error) {

	i.log.Info("---DeleteBranch------>", logger.Any("req", req))

	resp, err = i.strg.Branch().Delete(ctx, &content_service.BranchPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!DeleteBranch->Branch->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
