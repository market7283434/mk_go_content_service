package service

import (
	"context"
	"fmt"

	"gitlab.com/market/mk_go_content_service/config"
	"gitlab.com/market/mk_go_content_service/genproto/content_service"
	"gitlab.com/market/mk_go_content_service/grpc/client"
	"gitlab.com/market/mk_go_content_service/pkg/logger"
	"gitlab.com/market/mk_go_content_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type StaffService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*content_service.UnimplementedStaffServiceServer
}

func NewStaffService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *StaffService {
	return &StaffService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (i *StaffService) Create(ctx context.Context, req *content_service.CreateStaff) (resp *content_service.Staff, err error) {
	i.log.Info("---------CreateStaff---------", logger.Any("req", req))

	id, err := i.strg.Staff().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateStaff->Staff->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Staff().GetByID(ctx, id)
	if err != nil {
		i.log.Error("!!!GetStaffById --> Create Response->Staff", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *StaffService) GetByID(ctx context.Context, req *content_service.StaffPrimaryKey) (resp *content_service.Staff, err error) {

	i.log.Info("---------GetStaffById---------", logger.Any("req", req))

	fmt.Println(req.Id)
	resp, err = i.strg.Staff().GetByID(ctx,req)
	if err != nil {
		i.log.Error("!!!GetStaffById->Staff->GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}


func (i *StaffService) GetList(ctx context.Context, req *content_service.GetListStaffRequest) (resp *content_service.GetListStaffResponse, err error) {

	i.log.Info("---GetStaffs------>", logger.Any("req", req))

	resp, err = i.strg.Staff().GetList(ctx, req)
	if err != nil {
		i.log.Error("!!!GetStaffs->Staff->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *StaffService) Update(ctx context.Context, req *content_service.UpdateStaff) (resp *content_service.Staff, err error) {

	i.log.Info("---UpdateStaff------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Staff().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateStaff--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Staff().GetByID(ctx, &content_service.StaffPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetStaff->Staff->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *StaffService) Delete(ctx context.Context, req *content_service.StaffPrimaryKey) (resp *content_service.StaffEmpty, err error) {

	i.log.Info("---DeleteStaff------>", logger.Any("req", req))

	resp, err = i.strg.Staff().Delete(ctx, &content_service.StaffPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!DeleteStaff->Staff->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
