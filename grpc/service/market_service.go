package service

import (
	"context"

	"gitlab.com/market/mk_go_content_service/config"
	"gitlab.com/market/mk_go_content_service/genproto/content_service"
	"gitlab.com/market/mk_go_content_service/grpc/client"
	"gitlab.com/market/mk_go_content_service/pkg/logger"
	"gitlab.com/market/mk_go_content_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type MarketService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*content_service.UnimplementedMarketServiceServer
}

func NewMarketService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *MarketService {
	return &MarketService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (i *MarketService) Create(ctx context.Context, req *content_service.CreateMarket) (resp *content_service.Market, err error) {
	i.log.Info("---------CreateMarket---------", logger.Any("req", req))

	id, err := i.strg.Market().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateMarket->Market->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Market().GetByID(ctx, id)
	if err != nil {
		i.log.Error("!!!GetMarketById --> Create Response->Market", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *MarketService) GetByID(ctx context.Context, req *content_service.MarketPrimaryKey) (resp *content_service.Market, err error) {
	i.log.Info("---------GetMarketById---------", logger.Any("req", req))

	resp, err = i.strg.Market().GetByID(ctx, req)
	if err != nil {
		i.log.Error("!!!GetMarketById->Market->GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *MarketService) GetList(ctx context.Context, req *content_service.GetListMarketRequest) (resp *content_service.GetListMarketResponse, err error) {

	i.log.Info("---GetMarkets------>", logger.Any("req", req))

	resp, err = i.strg.Market().GetList(ctx, req)
	if err != nil {
		i.log.Error("!!!GetMarkets->Market->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *MarketService) Update(ctx context.Context, req *content_service.UpdateMarket) (resp *content_service.Market, err error) {

	i.log.Info("---UpdateMarket------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Market().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateMarket--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Market().GetByID(ctx, &content_service.MarketPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetMarket->Market->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *MarketService) Delete(ctx context.Context, req *content_service.MarketPrimaryKey) (resp *content_service.MarketEmpty, err error) {

	i.log.Info("---DeleteMarket------>", logger.Any("req", req))

	resp, err = i.strg.Market().Delete(ctx, &content_service.MarketPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!DeleteMarket->Market->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
