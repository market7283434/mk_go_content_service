package service

import (
	"context"

	"gitlab.com/market/mk_go_content_service/config"
	"gitlab.com/market/mk_go_content_service/genproto/content_service"
	"gitlab.com/market/mk_go_content_service/grpc/client"
	"gitlab.com/market/mk_go_content_service/pkg/logger"
	"gitlab.com/market/mk_go_content_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DillerService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*content_service.UnimplementedDillerServiceServer
}

func NewDillerService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *DillerService {
	return &DillerService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (i *DillerService) Create(ctx context.Context, req *content_service.CreateDiller) (resp *content_service.Diller, err error) {
	i.log.Info("---------CreateDiller---------", logger.Any("req", req))

	id, err := i.strg.Diller().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateDiller->Diller->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Diller().GetByID(ctx, id)
	if err != nil {
		i.log.Error("!!!GetDillerById --> Create Response->Diller", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *DillerService) GetByID(ctx context.Context, req *content_service.DillerPrimaryKey) (resp *content_service.Diller, err error) {
	i.log.Info("---------GetDillerById---------", logger.Any("req", req))

	resp, err = i.strg.Diller().GetByID(ctx, req)
	if err != nil {
		i.log.Error("!!!GetDillerById->Diller->GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *DillerService) GetList(ctx context.Context, req *content_service.GetListDillerRequest) (resp *content_service.GetListDillerResponse, err error) {

	i.log.Info("---GetDillers------>", logger.Any("req", req))

	resp, err = i.strg.Diller().GetList(ctx, req)
	if err != nil {
		i.log.Error("!!!GetDillers->Diller->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *DillerService) Update(ctx context.Context, req *content_service.UpdateDiller) (resp *content_service.Diller, err error) {

	i.log.Info("---UpdateDiller------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Diller().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateDiller--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Diller().GetByID(ctx, &content_service.DillerPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetDiller->Diller->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *DillerService) Delete(ctx context.Context, req *content_service.DillerPrimaryKey) (resp *content_service.DillerEmpty, err error) {

	i.log.Info("---DeleteDiller------>", logger.Any("req", req))

	resp, err = i.strg.Diller().Delete(ctx, &content_service.DillerPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!DeleteDiller->Diller->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
