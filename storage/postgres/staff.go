package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_content_service/genproto/content_service"
	"gitlab.com/market/mk_go_content_service/pkg/helper"
)

type staffRepo struct {
	db *pgxpool.Pool
}

func NewStaffRepo(db *pgxpool.Pool) *staffRepo {
	return &staffRepo{
		db: db,
	}
}

func (c *staffRepo) Create(ctx context.Context, req *content_service.CreateStaff) (resp *content_service.StaffPrimaryKey, err error) {
	var id = uuid.New().String()
	query := `INSERT INTO "staff" (
				id,
				first_name,
				last_name,
				phone_number,
				login,
				password,
				type_staff,
				created_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7,NOW())
	`
	_, err = c.db.Exec(ctx,
		query,
		id,
		req.FirstName,
		req.LastName,
		req.PhoneNumber,
		req.Login,
		req.Password,
		req.TypeStaff,
	)
	if err != nil {
		return nil, err
	}

	return &content_service.StaffPrimaryKey{Id: id}, nil

}

func (c *staffRepo) GetByID(ctx context.Context, req *content_service.StaffPrimaryKey) (resp *content_service.Staff, err error) {
	if len(req.Login) > 0 {
		err := c.db.QueryRow(ctx, "SELECT id FROM staff WHERE login = $1", req.Login).Scan(&req.Id)
		if err != nil {
			return nil, err

		}
	}

	query := `
		SELECT
			id,
			first_name,
			last_name,
			phone_number,
			login,
			password,
			type_staff,
			created_at,
			updated_at
		FROM "staff"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		first_name   sql.NullString
		last_name    sql.NullString
		phone_number sql.NullString
		login        sql.NullString
		password     sql.NullString
		type_staff   sql.NullString
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&first_name,
		&last_name,
		&phone_number,
		&login,
		&password,
		&type_staff,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &content_service.Staff{
		Id:          id.String,
		FirstName:   first_name.String,
		LastName:    last_name.String,
		PhoneNumber: phone_number.String,
		Login:       login.String,
		Password:    password.String,
		TypeStaff:   type_staff.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *staffRepo) GetList(ctx context.Context, req *content_service.GetListStaffRequest) (resp *content_service.GetListStaffResponse, err error) {

	resp = &content_service.GetListStaffResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			first_name,
			last_name,
			phone_number,
			login,
			password,
			type_staff,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "staff"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if req.LastName != "" {
		filter += ` AND last_name = '` + req.LastName + `'`
	}

	if req.FirstName != "" {
		filter += ` AND first_name ILIKE '%' || '` + req.FirstName + `' || '%'`
	}

	if req.PhoneNumber != "" {
		filter += ` AND phone_number ILIKE '%' || '` + req.PhoneNumber + `' || '%'`
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			first_name   sql.NullString
			last_name    sql.NullString
			phone_number sql.NullString
			login        sql.NullString
			password     sql.NullString
			type_staff   sql.NullString
			createdAt    sql.NullString
			updatedAt    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&first_name,
			&last_name,
			&phone_number,
			&login,
			&password,
			&type_staff,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Staffs = append(resp.Staffs, &content_service.Staff{
			Id:          id.String,
			FirstName:   first_name.String,
			LastName:    last_name.String,
			PhoneNumber: phone_number.String,
			Login:       login.String,
			Password:    password.String,
			TypeStaff:   type_staff.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *staffRepo) Update(ctx context.Context, req *content_service.UpdateStaff) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "staff"
			SET
				first_name = :first_name,
				last_name = :last_name,
				phone_number = :phone_number,
				login = :login,
				password = :password,
				type_staff= :type_staff,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"first_name":   req.GetFirstName(),
		"last_name":    req.GetLastName(),
		"phone_number": req.GetPhoneNumber(),
		"login":        req.GetLogin(),
		"password":     req.GetPassword(),
		"type_staff":   req.GetTypeStaff(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *staffRepo) Delete(ctx context.Context, req *content_service.StaffPrimaryKey) (*content_service.StaffEmpty, error) {

	query := `DELETE FROM "staff" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &content_service.StaffEmpty{}, nil
}
