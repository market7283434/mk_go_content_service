package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_content_service/genproto/content_service"
	"gitlab.com/market/mk_go_content_service/pkg/helper"
)

type dillerRepo struct {
	db *pgxpool.Pool
}

func NewDillerRepo(db *pgxpool.Pool) *dillerRepo {
	return &dillerRepo{
		db: db,
	}
}

func (c *dillerRepo) Create(ctx context.Context, req *content_service.CreateDiller) (resp *content_service.DillerPrimaryKey, err error) {
	var id = uuid.New().String()
	query := `INSERT INTO "diller" (
				id,
				first_name,
				last_name,
				phone_number,
				status,
				created_at
			) VALUES ($1, $2, $3, $4, $5,NOW())
	`
	_, err = c.db.Exec(ctx,
		query,
		id,
		req.FirstName,
		req.LastName,
		req.PhoneNumber,
		req.Status,
	)
	if err != nil {
		return nil, err
	}

	return &content_service.DillerPrimaryKey{Id: id}, nil

}

func (c *dillerRepo) GetByID(ctx context.Context, req *content_service.DillerPrimaryKey) (resp *content_service.Diller, err error) {
	query := `
		SELECT
			id,
			first_name,
			last_name,
			phone_number,
			status,
			created_at,
			updated_at
		FROM "diller"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		first_name   sql.NullString
		last_name    sql.NullString
		phone_number sql.NullString
		status       sql.NullString
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&first_name,
		&last_name,
		&phone_number,
		&status,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &content_service.Diller{
		Id:          id.String,
		FirstName:   first_name.String,
		LastName:    last_name.String,
		PhoneNumber: phone_number.String,
		Status:      status.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *dillerRepo) GetList(ctx context.Context, req *content_service.GetListDillerRequest) (resp *content_service.GetListDillerResponse, err error) {

	resp = &content_service.GetListDillerResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			first_name,
			last_name,
			phone_number,
			status,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "diller"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}


	if req.LastName != "" {
		filter += ` AND last_name = '` + req.LastName + `'`
	}


	if req.FirstName != "" {
		filter += ` AND first_name ILIKE '%' || '` + req.FirstName + `' || '%'`
	}


	if req.PhoneNumber != "" {
		filter += ` AND phone_number ILIKE '%' || '` + req.PhoneNumber + `' || '%'`
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			first_name   sql.NullString
			last_name    sql.NullString
			phone_number sql.NullString
			status       sql.NullString
			createdAt    sql.NullString
			updatedAt    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&first_name,
			&last_name,
			&phone_number,
			&status,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Dillers = append(resp.Dillers, &content_service.Diller{
			Id:          id.String,
			FirstName:   first_name.String,
			LastName:    last_name.String,
			PhoneNumber: phone_number.String,
			Status:      status.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *dillerRepo) Update(ctx context.Context, req *content_service.UpdateDiller) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "diller"
			SET
				first_name = :first_name,
				last_name = :last_name,
				phone_number = :phone_number,
				status= :status,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"first_name":   req.GetFirstName(),
		"last_name":    req.GetLastName(),
		"phone_number": req.GetPhoneNumber(),
		"status":  req.GetStatus(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *dillerRepo) Delete(ctx context.Context, req *content_service.DillerPrimaryKey) (*content_service.DillerEmpty, error) {

	query := `DELETE FROM "diller" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &content_service.DillerEmpty{}, nil
}
