package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_content_service/config"
	"gitlab.com/market/mk_go_content_service/storage"
)

type Store struct {
	db     *pgxpool.Pool
	branch *branchRepo
	market  *marketRepo
	staff   *staffRepo
	diller *dillerRepo
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {

	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",

			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)

	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err

}
func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Branch() storage.BranchRepoI {

	if s.branch == nil {
		s.branch = NewBranchRepo(s.db)
	}

	return s.branch

}

func (s *Store) Market() storage.MarketRepoI {

	if s.market == nil {
		s.market = NewMarketRepo(s.db)
	}

	return s.market

}


func (s *Store) Staff() storage.StaffRepoI {

	if s.staff == nil {
		s.staff = NewStaffRepo(s.db)
	}

	return s.staff

}

func (s *Store) Diller() storage.DillerRepoI {

	if s.diller == nil {
		s.diller = NewDillerRepo(s.db)
	}

	return s.diller

}