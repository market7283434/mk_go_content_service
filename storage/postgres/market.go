package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_content_service/genproto/content_service"
	"gitlab.com/market/mk_go_content_service/pkg/helper"
)

type marketRepo struct {
	db *pgxpool.Pool
}

func NewMarketRepo(db *pgxpool.Pool) *marketRepo {
	return &marketRepo{
		db: db,
	}
}

func (r *marketRepo) Create(ctx context.Context, req *content_service.CreateMarket) (resp *content_service.MarketPrimaryKey, err error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "market"(id, name, branch_id, created_at)
		VALUES ($1, $2, $3, NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		req.Name,
		req.BranchId,
	)

	if err != nil {
		return nil, err
	}

	return &content_service.MarketPrimaryKey{Id: id}, nil
}

func (r *marketRepo) GetByID(ctx context.Context, req *content_service.MarketPrimaryKey) (*content_service.Market, error) {
	var (
		query string

		id         sql.NullString
		name       sql.NullString
		branch_id  sql.NullString
		created_at sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			branch_id,
			created_at
		FROM "market"
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&branch_id,
		&created_at,
	)

	if err != nil {
		return nil, err
	}

	fmt.Println(req.Id)

	return &content_service.Market{
		Id:        id.String,
		Name:      name.String,
		BranchId:  branch_id.String,
		CreatedAt: created_at.String,
	}, nil
}

func (r *marketRepo) GetList(ctx context.Context, req *content_service.GetListMarketRequest) (*content_service.GetListMarketResponse, error) {

	var (
		resp   = &content_service.GetListMarketResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			branch_id,
			created_at,
			updated_at
		FROM "market"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Name != "" {
		where += ` AND name ILIKE '%' || '` + req.Name + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var (
			id         sql.NullString
			name       sql.NullString
			branch_id  sql.NullString
			created_at sql.NullString
			updated_at sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&branch_id,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Markets = append(resp.Markets, &content_service.Market{
			Id:        id.String,
			Name:      name.String,
			BranchId:  branch_id.String,
			CreatedAt: created_at.String,
			UpdatedAt: updated_at.String,
		})
	}

	return resp, nil
}

func (r *marketRepo) Update(ctx context.Context, req *content_service.UpdateMarket) (int64, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"market"
		SET
			name = :name,
			branch_id = :branch_id,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"name":         req.Name,
		"branch_id":      req.BranchId,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (r *marketRepo) Delete(ctx context.Context, req *content_service.MarketPrimaryKey) (*content_service.MarketEmpty, error) {

	_, err := r.db.Exec(ctx, "DELETE FROM market WHERE id = $1", req.Id)
	if err != nil {
		return nil,err
	}

	return &content_service.MarketEmpty{},nil
}
