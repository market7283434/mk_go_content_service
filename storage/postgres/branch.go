package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market/mk_go_content_service/genproto/content_service"
	"gitlab.com/market/mk_go_content_service/pkg/helper"
)

type branchRepo struct {
	db *pgxpool.Pool
}

func NewBranchRepo(db *pgxpool.Pool) *branchRepo {
	return &branchRepo{
		db: db,
	}
}

func (r *branchRepo) Create(ctx context.Context, req *content_service.CreateBranch) (resp *content_service.BranchPrimaryKey, err error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "branch"(id, name, address ,phone_number, created_at)
		VALUES ($1, $2, $3, $4, NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		req.Name,
		req.Address,
		req.PhoneNumber,
	)

	if err != nil {
		return nil, err
	}

	return &content_service.BranchPrimaryKey{Id: id}, nil
}

func (r *branchRepo) GetByID(ctx context.Context, req *content_service.BranchPrimaryKey) (*content_service.Branch, error) {
	var (
		query string

		id          sql.NullString
		name        sql.NullString
		address     sql.NullString
		phoneNumber sql.NullString
		created_at  sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			address,
			phone_number,
			created_at
		FROM "branch"
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&address,
		&phoneNumber,
		&created_at,
	)

	if err != nil {
		return nil, err
	}

	fmt.Println(req.Id)

	return &content_service.Branch{
		Id:          id.String,
		Name:        name.String,
		Address:     address.String,
		PhoneNumber: phoneNumber.String,
		CreatedAt:   created_at.String,
	}, nil
}

func (r *branchRepo) GetList(ctx context.Context, req *content_service.GetListBranchRequest) (*content_service.GetListBranchResponse, error) {

	var (
		resp   = &content_service.GetListBranchResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			address,
			phone_number,
			created_at,
			updated_at
		FROM "branch"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Name != "" {
		where += ` AND name ILIKE '%' || '` + req.Name + `' || '%'`
	}

	if req.PhoneNumber != "" {
		where += ` AND phone_number ILIKE '%' || '` + req.PhoneNumber + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var (
			id          sql.NullString
			name        sql.NullString
			address     sql.NullString
			phoneNumber sql.NullString
			created_at  sql.NullString
			updated_at  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&address,
			&phoneNumber,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Branchs = append(resp.Branchs, &content_service.Branch{
			Id:          id.String,
			Name:        name.String,
			Address:     address.String,
			PhoneNumber: phoneNumber.String,
			CreatedAt:   created_at.String,
			UpdatedAt:   updated_at.String,
		})
	}

	return resp, nil
}

func (r *branchRepo) Update(ctx context.Context, req *content_service.UpdateBranch) (int64, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"branch"
		SET
			name = :name,
			address = :address,
			phone_number = :phone_number,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"name":         req.Name,
		"address":      req.Address,
		"phone_number": req.PhoneNumber,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (r *branchRepo) Delete(ctx context.Context, req *content_service.BranchPrimaryKey) (*content_service.BranchEmpty, error) {

	_, err := r.db.Exec(ctx, "DELETE FROM branch WHERE id = $1", req.Id)
	if err != nil {
		return nil, err
	}

	return &content_service.BranchEmpty{},nil
}
