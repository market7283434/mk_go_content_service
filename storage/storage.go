package storage

import (
	"context"

	"gitlab.com/market/mk_go_content_service/genproto/content_service"
)

type StorageI interface {
	CloseDB()
	Branch() BranchRepoI
	Market()  MarketRepoI
	Staff()  StaffRepoI
	Diller()  DillerRepoI
}

type BranchRepoI interface {
	Create(ctx context.Context, req *content_service.CreateBranch) (resp *content_service.BranchPrimaryKey, err error)
	GetByID(ctx context.Context, req *content_service.BranchPrimaryKey) (resp *content_service.Branch, err error)
	GetList(ctx context.Context, req *content_service.GetListBranchRequest) (resp *content_service.GetListBranchResponse, err error)
	Update(ctx context.Context, req *content_service.UpdateBranch) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *content_service.BranchPrimaryKey) (*content_service.BranchEmpty, error)
}

type MarketRepoI interface {
	Create(ctx context.Context, req *content_service.CreateMarket) (resp *content_service.MarketPrimaryKey, err error)
	GetByID(ctx context.Context, req *content_service.MarketPrimaryKey) (resp *content_service.Market, err error)
	GetList(ctx context.Context, req *content_service.GetListMarketRequest) (resp *content_service.GetListMarketResponse, err error)
	Update(ctx context.Context, req *content_service.UpdateMarket) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *content_service.MarketPrimaryKey) (*content_service.MarketEmpty, error)
}



type StaffRepoI interface {
	Create(ctx context.Context, req *content_service.CreateStaff) (resp *content_service.StaffPrimaryKey, err error)
	GetByID(ctx context.Context, req *content_service.StaffPrimaryKey) (resp *content_service.Staff, err error)
	GetList(ctx context.Context, req *content_service.GetListStaffRequest) (resp *content_service.GetListStaffResponse, err error)
	Update(ctx context.Context, req *content_service.UpdateStaff) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *content_service.StaffPrimaryKey) (*content_service.StaffEmpty, error)
}


type DillerRepoI interface {
	Create(ctx context.Context, req *content_service.CreateDiller) (resp *content_service.DillerPrimaryKey, err error)
	GetByID(ctx context.Context, req *content_service.DillerPrimaryKey) (resp *content_service.Diller, err error)
	GetList(ctx context.Context, req *content_service.GetListDillerRequest) (resp *content_service.GetListDillerResponse, err error)
	Update(ctx context.Context, req *content_service.UpdateDiller) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *content_service.DillerPrimaryKey) (*content_service.DillerEmpty, error)
}

